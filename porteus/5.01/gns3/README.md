GNS3
====

## Links
Website: https://www.gns3.com/
Documentation: https://docs.gns3.com/docs/

## Description
Build, Design and Test your network in a risk-free virtual environment and access the largest networking community to help. Whether you are studying for your first networking exam or building out a state-wide telecommunications network, GNS3 offers an easy way to design and build networks of any size without the need for hardware.

## About dependencies
- 0050-multilib is required for IOU templates
  - 0050-multilib for PorteuX stable works in Porteus
  - IOU appliances don't start anyway, investigating it...
- qemu is required for some templates
- wireshark is required for capturing packets

## About NAT bridge
libvirt is not included, instead added ``rc.gns3-nat`` script, which:
- creates bridge named like default libvirt one (virbr0)
- adds NAT and filter rules
- starts DHCP server on bridge

Requires setting net.ipv4.conf.all.forwarding kernel variable value to 1 to make NAT work.
Note: this script is NOT executed during boot by default nor when this module is activating.
