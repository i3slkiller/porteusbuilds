#!/usr/bin/env xzmbuild
# Maintainer: i3slkiller <i3sl.1.3.3.0@gmail.com> (PGP fingerprint: 7B69 A55C 4533 F5E2 44C2  4889 095A 0992 0ECC A5AF)

xzmname=docker
version=${version:-24.0.9}
build=${build:-1}

url=https://www.docker.com/

arch=(x86_64)

tag=${tag:-_pb}
sbtag=${sbtag:-_pb}

requires=()
optional=()
makerequires=(05-devel 05-devel-extra-golang)
conflicts=()

validpgpkeys=()

options=(net)

sb_sbo_commit=ca3cfecb6f0115a65c4c9ecc03fa1f7aa1c840a3
sb_packages=(
  sbo:system/runc:1.1.12-1:native
  sbo:system/containerd:1.7.14-1:native
  sbo:system/tini:0.19.0-1:native
  sbo:system/docker-proxy:20231021_3797618-1:native
  sbo:system/docker-compose:2.26.0-1:native
  sbo:system/docker-cli:24.0.9-1:native
  sbo:system/docker:$version-1:native
)
sb_files=(
  skip
  skip
  skip
  skip
  'doinst.sh'
  skip
  'doinst.sh config/docker.default config/docker.logrotate config/rc.docker'
)
sb_sources=(
  https://github.com/opencontainers/runc/archive/v_VER_/runc-_VER_.tar.gz
  https://github.com/containerd/containerd/archive/v_VER_/containerd-_VER_.tar.gz
  https://github.com/krallin/tini/archive/v_VER_/tini-_VER_.tar.gz
  https://github.com/moby/libnetwork/archive/3797618f9a38372e8107d8c06f6ae199e1133ae8/libnetwork-3797618f9a38372e8107d8c06f6ae199e1133ae8.tar.gz
  https://github.com/docker/compose/archive/v_VER_/compose-_VER_.tar.gz
  https://github.com/docker/cli/archive/v_VER_/cli-_VER_.tar.gz
  https://github.com/moby/moby/archive/v_VER_/moby-_VER_.tar.gz
)
sb_sources_checksums=(
  sha512:1.1.12:92e8ac54a77d7ebcc76b5a9cc08d9a064211f12e9d26f064070cc203a5afb11c3af28d8f556f297513f797a2933d50bf10a8f22e307724041d66aa8c5ca1d9d3
  sha512:1.7.14:c80ad36027407b2e06fdff76280750f84de8d7300ef8be275976766f2a0a04dec1f0f850c8efcceaa7f6163f43922b427d7ae1fcdeabfaf531f487c25c461dc8
  sha512:0.19.0:1fa85b56e2c6085ea474f251928e7a40510d92aeef60b3c145b0496969c1b5df86835d143cb91ef5b4bf4da63fa8a56947cc39a4276e4b72faa57276d432b292
  sha512:20231021_3797618:c162b5e520c49579ca3428937ac7d453dc501be582aa4ed6437676552dd44715d9abbb5d043ca198a8b74d3798c90f53576da751041f0f9e5fcc2806c4f8486d
  sha512:2.26.0:8a3d8fb8c6812e85bdaebbcfb7a4976df46271e69fb4d7e9bc7af78c39833195d113dfb8e155f5c4b4f30f86e17a823989cb35a966069123e53779eb58a9ab38
  sha512:24.0.9:7abfbf593783ffaadf84461b7e6dcbef7fbb857166721ba8004531212a231f4630a747c09ef8a3a5cf119861c51465ba3d5bc4b63f0e4d76936fd3b1baff530f
  sha512:24.0.9:b71a058f32fb80676bb4c83f5d2236c9496ffc5c7f216ebff5bcac6f5959e121be3b2bfd2ff9aa5cccee27f71947dfe5b76090e82020806cc9ee452cd1f21084
)

prepare() {
  . "/etc/profile.d/go.sh"
}

install_packages=(runc containerd tini docker-proxy docker-compose docker-cli docker)
