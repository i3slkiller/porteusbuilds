firetools
=========

## Links
Website: https://firejailtools.wordpress.com/

## Description
Firetools is the graphical user interface of Firejail security sandbox. It provides a sandbox launcher integrated with the system tray, sandbox editing, management and statistics.
