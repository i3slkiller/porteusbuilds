DDNet
=====

## Links
Website: https://ddnet.org/
Wiki: https://wiki.ddnet.org/

## Description
DDraceNetwork (DDNet) is an actively maintained version of DDRace, a [Teeworlds](https://www.teeworlds.com/) modification with a unique cooperative gameplay. Help each other play through [custom maps](https://ddnet.org/releases/) with up to 64 players, compete against the best in [international tournaments](https://ddnet.org/tournaments/), design your [own maps](https://ddnet.org/howto/), or run your [own server](https://ddnet.org/settingscommands/). The [official servers](https://ddnet.org/status/) are around the world. All [ranks](https://ddnet.org/ranks/) made on official servers are available worldwide and you can [collect points](https://ddnet.org/players/)!
