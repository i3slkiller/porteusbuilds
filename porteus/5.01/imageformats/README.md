imageformats
============

## Description
Support for image formats such as EXR, RAW, JXL and AVIF/HEIF in programs like Gimp or Krita (must be compiled into those).

This PorteusBuild script builds these modules:
- imageformats - image format libraries used by programs
- imageformats-dev - files used to compile image formats support
