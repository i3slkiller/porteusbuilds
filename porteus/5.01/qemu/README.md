QEMU
====

## Links
Website: https://www.qemu.org/
Documentation: https://www.qemu.org/documentation

## Description
A generic and open source machine emulator and virtualizer.

## Notes
This script contains ``/etc/rc.d/init.d/rc.qemu-set-dev-kvm-permissions`` script executed only during module activation on runtime, which sets group to ``users`` and permission to ``660`` for ``/dev/kvm`` if exists.
