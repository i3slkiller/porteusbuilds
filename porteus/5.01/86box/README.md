86Box
=====

## Links
Website: https://86box.net/
Documentation: https://86box.readthedocs.io/en/latest/index.html

## Description
86Box is a low level x86 emulator that runs older operating systems and software designed for IBM PC systems and compatibles from 1981 through fairly recent system designs based on the PCI bus.

### Features
- Easy to use interface inspired by mainstream hypervisor software
- Low level emulation of 8086-based processors up to the Pentium with focus on accuracy
- Great range of customizability of virtual machines
- Many available systems, such as the very first IBM PC 5150 from 1981, or the more obscure IBM PS/2 line of systems based on the Micro Channel Architecture
- Lots of supported peripherals including video adapters, sound cards, network adapters, hard disk controllers, and SCSI adapters
- MIDI output to Windows built-in MIDI support, FluidSynth, or emulated Roland synthesizers
- Supports running MS-DOS, older Windows versions, OS/2, many Linux distributions, or vintage systems such as BeOS or NEXTSTEP, and applications for these systems

### Minimum system requirements and recommendations
- Intel Core 2 or AMD Athlon 64 processor
- 4 GB of RAM
- Windows version: Windows 7 Service Pack 1, Windows 8.1 or Windows 10
- macOS version: macOS High Sierra 10.13
- Linux version: Ubuntu 16.04, Debian 9.0 or other distributions from 2016 onwards

Performance may vary depending on both host and guest configuration. Most emulation logic is executed in a single thread; therefore, systems with better IPC (instructions per clock) generally should be able to emulate higher clock speeds.
