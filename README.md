Porteus module build scripts
============================

```
NOTE: These scripts have not been tested in 32-bit system.
```

## How to use
- select program you want to build
- download all files needed for build and build it ``xzmbuild``

## Notes
It's recommended to build modules on virtual machine or bare metal system with ``baseonly`` parameter and without other modules than required for build (the exception is VirtualBox Guest Additions, which can be used to share files between host and guest).

## Package sources
| Name | Link |
|------|------|
| slackware | https://ftp.slackware.pl/pub/slackware/slackware64-15.0/slackware64/ |
| slackware patches | https://ftp.slackware.pl/pub/slackware/slackware64-15.0/patches/ |

## Slackbuild sources
| Name | Link |
|------|------|
| slackware | https://ftp.slackware.pl/pub/slackware/slackware64-15.0/source/ |
| slackbuilds.org | https://slackbuilds.org/ |
| alienbob | http://www.slackware.com/~alien/slackbuilds/ |
| pbek/qownnotes-slackbuilds (github) | https://github.com/pbek/qownnotes-slackbuilds/ |
| conraid | https://github.com/conraid/SlackBuilds |
| own and modified | https://gitlab.com/i3slkiller/slackbuilds |
