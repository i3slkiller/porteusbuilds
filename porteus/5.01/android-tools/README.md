android-tools
=============

## Links
Website: https://github.com/nmeum/android-tools

## Description
These are the adb, append2simg, fastboot, img2simg, mke2fs.android and simg2img tools from the android sdk.
