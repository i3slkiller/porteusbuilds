cgroupfs-scripts
================

- cgroupfs-mount - mounts cgroup1 pseudofilesystems listed in /proc/cgroups and cgroup2 unified one
- cgroupfs-umount - unmounts the above one
