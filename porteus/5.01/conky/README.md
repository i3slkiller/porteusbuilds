conky
=====

## Links
Website: https://github.com/brndnmtthws/conky
Documentation: https://conky.cc/
Wiki: https://github.com/brndnmtthws/conky/wiki

## Description
**Conky** is a free, light-weight system monitor for X, that displays any kind of information on your desktop. It can also run on Wayland, macOS, output to your console, a file, or even HTTP (oh my!).

### Features
Conky can display more than 300 built-in objects, including support for:

- A plethora of OS stats (uname, uptime, **CPU usage**, **mem usage**, disk usage, **"top"** like process stats, and **network monitoring**, just to name a few).
- Built-in **IMAP** and **POP3** support.
- Built-in support for many popular music players ([MPD](https://musicpd.org/), [XMMS2](https://github.com/xmms2/wiki/wiki), [Audacious](https://audacious-media-player.org/)).
- Can be extended using built-in [**Lua**](https://github.com/brndnmtthws/conky/blob/main/lua) support, or any of your own scripts and programs ([more](https://github.com/brndnmtthws/conky/wiki#tutorial)).
- Built-in [**Imlib2**](https://docs.enlightenment.org/api/imlib2/html/) and [**Cairo**](https://www.cairographics.org/) bindings for arbitrary drawing with Lua ([more](https://github.com/brndnmtthws/conky/wiki/Lua)).
- Runs on Linux, FreeBSD, OpenBSD, DragonFlyBSD, NetBSD, Solaris, Haiku, and macOS!
- [Docker](https://hub.docker.com/r/brndnmtthws/conky/) image available for amd64, armv7, and aarch64 (aka armv8)

... and much much more.

Conky can display information either as text, or using simple progress bars and graph widgets, with different fonts and colours, as well as handle [mouse events](https://github.com/brndnmtthws/conky/wiki/Mouse-Events).
