KeepassXC
=========

## Links
Website: https://keepassxc.org/
Documentation: https://keepassxc.org/docs/

## Description
KeePassXC is a modern, secure, and open-source password manager that stores and manages your most sensitive information.
