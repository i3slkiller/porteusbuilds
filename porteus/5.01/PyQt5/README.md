PyQt5
=====

## Links
Website: https://riverbankcomputing.com/software/pyqt/
Wiki: https://wiki.python.org/moin/PyQt

## Description
PyQt5 is a set of Python bindings for Trolltech's Qt5 application framework and runs on all platforms supported by Qt5.
