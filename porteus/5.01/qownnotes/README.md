QOwnNotes
=========

## Links
Website: https://www.qownnotes.org/

## Description
QOwnNotes is a free and customizable application that lets you store your notes as plain-text markdown files on your computer. You can sync notes across devices using sync services like Nextcloud or ownCloud, and enjoy a small footprint and fast performance.
