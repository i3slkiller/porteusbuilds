sdl12-compat
============

## Links
Website: https://github.com/libsdl-org/sdl12-compat

## Description
This is the Simple DirectMedia Layer, a general API that provides low level access to audio, keyboard, mouse, joystick, 3D hardware via OpenGL, and 2D framebuffer across multiple platforms.

This code is a compatibility layer; it provides a binary and source compatible API for programs written against SDL 1.2, but it uses SDL 2.0 behind the scenes.
