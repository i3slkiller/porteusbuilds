SuperTux
========

## Links
Website: https://www.supertux.org/
Wiki: https://github.com/SuperTux/supertux/wiki

## Description
SuperTux is a jump'n'run game with strong inspiration from the Super Mario Bros. games for the various Nintendo platforms.

Run and jump through multiple worlds, fighting off enemies by jumping on them, bumping them from below or tossing objects at them, grabbing power-ups and other stuff on the way.
