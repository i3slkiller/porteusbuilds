mumble
======

## Links
Website: https://www.mumble.info/
Documentation: https://www.mumble.info/documentation/

## Description
Mumble is a voice chat application for groups. While it can be used for any kind of activity, it is primarily intended for gaming. It can be compared to programs like Ventrilo or TeamSpeak.
