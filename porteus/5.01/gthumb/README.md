gThumb
======

## Links
Website: https://wiki.gnome.org/action/show/Apps/Gthumb

## Description
gThumb is an image viewer and browser for the GNOME Desktop. It also includes an importer tool for transferring photos from cameras.

## Fixed issues
- ~~Reading HEIF images requires 05-devel module activated~~ Mime database didn't refreshed when module loaded, that's why HEIF images didn't work
- fixed HEIF and AVIF file associations
