scrcpy
======

## Links
Website: https://github.com/Genymobile/scrcpy

## Description
This application mirrors Android devices (video and audio) connected via USB or over TCP/IP, and allows to control the device with the keyboard and the mouse of the computer. It does not require any root access. It works on Linux, Windows and macOS.

It focuses on:
- lightness: native, displays only the device screen
- performance: 30~120fps, depending on the device
- quality: 1920×1080 or above
- low latency: 35~70ms
- low startup time: ~1 second to display the first image
- non-intrusiveness: nothing is left installed on the Android device
- user benefits: no account, no ads, no internet required
- freedom: free and open source software

Its features include:
- audio forwarding (Android 11+)
- recording
- virtual display
- mirroring with Android device screen off
- copy-paste in both directions
- configurable quality
- camera mirroring (Android 12+)
- mirroring as a webcam (V4L2) (Linux-only)
- physical keyboard and mouse simulation (HID)
- gamepad support
- OTG mode
- and more…

### Prerequisites
The Android device requires at least API 21 (Android 5.0).

Audio forwarding is supported for API >= 30 (Android 11+).

Make sure you enabled USB debugging on your device(s).

On some devices (especially Xiaomi), you might get the following error:
```
java.lang.SecurityException: Injecting input events requires the caller (or the source of the instrumentation, if any) to have the INJECT_EVENTS permission.
```
In that case, you need to enable an additional option USB debugging (Security Settings) (this is an item different from USB debugging) to control it using a keyboard and mouse. Rebooting the device is necessary once this option is set.

Note that USB debugging is not required to run scrcpy in OTG mode.

### Usage examples
Capture the screen in H.265 (better quality), limit the size to 1920, limit the frame rate to 60fps, disable audio, and control the device by simulating a physical keyboard:
```
scrcpy --video-codec=h265 --max-size=1920 --max-fps=60 --no-audio --keyboard=uhid
scrcpy --video-codec=h265 -m1920 --max-fps=60 --no-audio -K  # short version
```

Start VLC in a new virtual display (separate from the device display):
```
scrcpy --new-display=1920x1080 --start-app=org.videolan.vlc
```

Record the device camera in H.265 at 1920x1080 (and microphone) to an MP4 file:
```
scrcpy --video-source=camera --video-codec=h265 --camera-size=1920x1080 --record=file.mp4
```

Capture the device front camera and expose it as a webcam on the computer (on Linux):
```
scrcpy --video-source=camera --camera-size=1920x1080 --camera-facing=front --v4l2-sink=/dev/video2 --no-playback
```

Control the device without mirroring by simulating a physical keyboard and mouse (USB debugging not required):
```
scrcpy --otg
```

Control the device using gamepad controllers plugged into the computer:
```
scrcpy --gamepad=uhid
scrcpy -G  # short version
```
