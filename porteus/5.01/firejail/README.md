firejail
========

## Links
Website: https://firejail.wordpress.com/
Documentation: https://firejail.wordpress.com/documentation-2/

## Description
**Firejail** is a SUID program that reduces the risk of security breaches by restricting the running environment of untrusted applications using [Linux namespaces](https://lwn.net/Articles/531114/) and [seccomp-bpf](https://l3net.wordpress.com/2015/04/13/firejail-seccomp-guide/). It allows a process and all its descendants to have their own private view of the globally shared kernel resources, such as the network stack, process table, mount table.

Firejail can sandbox any type of processes: servers, graphical applications, and even user login sessions. The software includes security profiles for a large number of Linux programs: Mozilla Firefox, Chromium, VLC, Transmission etc.

## Notes
This module creates file with blacklisted Porteus-specific folders automatically when this module loaded and every boot:
- /mnt/live
- /mnt/dm-0 if changes encrypted
- /opt/porteus-scripts
- Porteus data
- Porteus save file/folder, if set by ``changes`` cheatcode
- Porteus config
- custom rootcopy folder
- extra module folders
- magic folder targets

It also blacklist boot, efi, grub, syslinux and Windows bootloader files and folders on each drive root.
