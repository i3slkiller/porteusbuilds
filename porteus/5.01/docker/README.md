Docker
======

## Links
Website: https://www.docker.com/
Documentation: https://docs.docker.com/

## Description
Docker is an open-source project to easily create lightweight, portable, self-sufficient containers from any application. The same container that a developer builds and tests on a laptop can run at scale, in production, on VMs, bare metal, OpenStack clusters, public clouds and more.

## Notes
This module require kernel with these control groups compiled:
- cpuset
- cpu
- cpuacct
- blkio
- memory
- devices
- freezer
- net_cls
- perf_event
- pids
- rdma
- misc

Otherwise daemon and/or containers may not start at all.

Note: some slackbuilds will download some files during build.
