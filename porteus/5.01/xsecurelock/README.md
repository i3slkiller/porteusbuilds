xsecurelock
===========

## Links
Website: https://github.com/google/xsecurelock

## Description
XSecureLock is an X11 screen lock utility designed with the primary goal of security.

Screen lock utilities are widespread. However, in the past they often had security issues regarding authentication bypass (a crashing screen locker would unlock the screen), information disclosure (notifications may appear on top of the screen saver), or sometimes even worse.

In XSecureLock, security is achieved using a modular design to avoid the usual pitfalls of screen locking utility design on X11.

## Notes
Configuration instructions are available at https://github.com/google/xsecurelock#setup and in /usr/doc/xsecurelock-1.9.0/README.md

How to set this screensaver:
- Xfce
  - ``xfconf-query -c xfce4-session -p /general/LockCommand -s xsecurelock``
- others
  - Set ``Win²+L`` shortcut to run ``xsecurelock``
  - Don't forget to disable old screensaver

² - ``Win`` is the key with Windows logo, in Linux it appears as ``Meta`` or ``Super``.
