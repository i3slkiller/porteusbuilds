Syncthing tray
==============

## Links
Website: https://github.com/Martchus/syncthingtray

## Description
This project contains the following integrations for Syncthing:
- Tray application (using the Qt framework)
- Context menu extension for the Dolphin file manager
- Plasmoid for KDE Plasma
- Command-line interface
- Qt-ish C++ library

### Features
- Provides quick access to most frequently used features but does not intend to replace the official web-based UI
  - Check state of folders and devices
  - Check current traffic statistics
  - Display further details about folders and devices, like last file, last scan, items out of sync, ...
  - Display ongoing downloads
  - Display Syncthing log
  - Trigger re-scan of a specific folder or all folders at once
  - Open a folder with the default file browser
  - Pause/resume a specific device or all devices at once
  - Pause/resume a specific folder
  - View recent history of changes (done locally and remotely)
- Shows "desktop" notifications
  - The events to show notifications for can be configured
  - Uses Qt's notification support or a D-Bus notification daemon directly
- Provides a wizard for a quick setup
- Allows monitoring the status of the Syncthing systemd unit and to start and stop it (see section Configuring systemd integration)
- Provides an option to conveniently add the tray to the applications launched when the desktop environment starts
- Can launch Syncthing automatically when started and display stdout/stderr (useful under Windows)
- Provides quick access to the official web-based UI
  - Can be opened as regular browser tab
  - Can be opened in a dedicated window utilizing either
  - - Qt WebEngine/WebKit
  - - the "app mode" of a Chromium-based browser (e.g. Chrome and Edge)
- Allows switching quickly between multiple Syncthing instances
- Also features a simple command line utility syncthingctl
  - Check status
  - Trigger rescan/pause/resume/restart
  - Wait for idle
  - View and modify raw configuration
  - Supports Bash completion, even for folder and device names
- Also bundles a KIO plugin which shows the status of a Syncthing folder and allows to trigger Syncthing actions in the Dolphin file manager
  - Rescan selected items
  - Rescan entire Syncthing folder
  - Pause/resume Syncthing folder
  - See also screenshots section
- Allows building Syncthing as a library to run it in the same process as the tray/GUI
- English and German localization

## Notes
Pass ``--setvar PLASMOID=yes`` and/or ``--setvar FILE_ITEM_ACTION_PLUGIN=yes`` arguments to ``xzmbuild`` for:
- downloading dependencies required for build plasmoid/file item action plugin for Dolphin
- building plasmoid/plugin
