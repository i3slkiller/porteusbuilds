VeraCrypt (old version)
=======================

## Links
Website: https://veracrypt.fr/en/Home.html
Documentation: https://veracrypt.fr/en/Documentation.html

## Description
Starting from version 1.26.7, VeraCrypt discontinued support for the TrueCrypt format to prioritize the highest security standards. However, recognizing the transitionary needs of users, there is preserved version 1.25.9, the last to support the TrueCrypt format.

VeraCrypt is a free disk encryption software brought to you by IDRIX (https://www.idrix.fr) and that is based on TrueCrypt 7.1a.

VeraCrypt adds enhanced security to the algorithms used for system and partitions encryption making it immune to new developments in brute-force attacks. VeraCrypt also solves many vulnerabilities and security issues found in TrueCrypt.
