OpenSnitch
==========

## Links
Website: https://github.com/evilsocket/opensnitch
Wiki: https://github.com/evilsocket/opensnitch/wiki

## Description
OpenSnitch is a GNU/Linux application firewall.

Key features:
- Interactive outbound connections filtering.
- [Block ads, trackers or malware domains](https://github.com/evilsocket/opensnitch/wiki/block-lists) system wide.
- Ability to [configure system firewall](https://github.com/evilsocket/opensnitch/wiki/System-rules) from the GUI (nftables).
  - Configure input policy, allow inbound services, etc.
- Manage [multiple nodes](https://github.com/evilsocket/opensnitch/wiki/Nodes) from a centralized GUI.
- [SIEM integration](https://github.com/evilsocket/opensnitch/wiki/SIEM-integration)

## Notes
To start opensnitchd service at boot:
- ``chmod +x /etc/rc.d/rc.opensnitchd``
- add ``[ -x /etc/rc.d/rc.opensnitchd ] && /etc/rc.d/rc.opensnitchd start`` in ``/etc/rc.d/rc.local``
