wxGTK3
======

## Links
Website: https://wxwidgets.org/

## Description
wxGTK3 is part of wxWidgets, a cross-platform API for writing GUI applications on multiple platforms that still utilize the native platform's controls and utilities.

## Notes
This module contains shared libraries only (required for some programs).
