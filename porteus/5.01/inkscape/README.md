inkscape
========

## Links
Website: https://inkscape.org/

## Description
Inkscape is a vector graphics editor application. Its stated goal is to become a powerful graphics tool while being fully compliant with the XML, SVG, and CSS standards.
