qt5-webkit
==========

## Links
Website: https://github.com/qtwebkit/qtwebkit

## Description
The Qt WebKit module provides the WebView API, which allows QML applications to render regions of dynamic web content. A WebView component may share the screen with other QML components or encompass the full screen as specified within the QML application.
