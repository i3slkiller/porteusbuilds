Krita
=====

## Links
Website: https://krita.org/
Documentation: https://docs.krita.org/en/

## Description
Krita is a professional FREE and open source painting program. It is made by artists that want to see affordable art tools for everyone.
- concept art
- texture and matte painters
- illustrations and comics
