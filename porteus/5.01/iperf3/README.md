iperf3
======

## Links
Website: https://github.com/esnet/iperf
Documentation: https://github.com/esnet/iperf/wiki

## Description
iperf is a tool for active measurements of the maximum achievable bandwidth on IP networks. It supports tuning of various parameters related to timing, protocols, and buffers. For each test it reports the measured throughput / bitrate, loss, and other parameters.
