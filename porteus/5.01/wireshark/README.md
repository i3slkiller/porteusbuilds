Wireshark
=========

## Links
Website: https://www.wireshark.org/
Documentation: https://www.wireshark.org/docs/

## Description
Wireshark is a network traffic analyzer, or "sniffer", for Linux, macOS, *BSD and other Unix and Unix-like operating systems and for Windows. It uses Qt, a graphical user interface library, and libpcap and npcap as packet capture and filtering libraries.

The Wireshark distribution also comes with TShark, which is a line-oriented sniffer (similar to Sun's snoop or tcpdump) that uses the same dissection, capture-file reading and writing, and packet filtering code as Wireshark, and with editcap, which is a program to read capture files and write the packets from that capture file, possibly in a different capture file format, and with some packets possibly removed from the capture.
