Stellarium
==========

## Links
Website: https://stellarium.org/
Wiki: https://github.com/Stellarium/stellarium/wiki/

## Description
Stellarium is a free open source planetarium for your computer. It shows a realistic sky in 3D, just like what you see with the naked eye, binoculars or a telescope.

### features
sky
- default catalogue of over 600,000 stars
- extra catalogues with more than 177 million stars
- default catalogue of over 80,000 deep-sky objects
- extra catalogue with more than 1 million deep-sky objects
- asterisms and illustrations of the constellations
- constellations for 40+ different cultures
- calendars of 35+ different cultures
- images of nebulae (full Messier catalogue)
- realistic Milky Way
- very realistic atmosphere, sunrise and sunset
- the planets and their satellites
- all-sky surveys (DSS, HiPS)

interface
- a powerful zoom
- time control
- multilingual interface
- scripting interface
- fisheye projection for planetarium domes
- spheric mirror projection for your own low-cost dome
- graphical interface and extensive keyboard control
- HTTP interface (web-based control, remote control API)
- telescope control

visualisation
- several coordinate grids
- precession circles
- star twinkling
- shooting stars
- tails of comets
- eclipse simulation
- supernovae and novae simulation
- exoplanet locations
- ocular view simulation
- 3D sceneries
- skinnable landscapes with spheric panorama projection

customizability
- plugin system adding artifical satellites, ocular simulation, telescope control and more
- ability to add new solar system objects from online resources...
- add your own deep sky objects, landscapes, constellation images, scripts...
