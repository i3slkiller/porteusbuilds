Crow Translate
==============

## Links
Website: https://crow-translate.github.io/

## Description
Crow Translate is a simple and lightweight translator written in C++ / Qt that allows you to translate and speak text using Google, Yandex, Bing, LibreTranslate and Lingva translate API.

### Features
- Translate and speak text from screen or selection
- Support 125 different languages
- Low memory consumption (~20MB)
- Highly customizable shortcuts
- Command-line interface with rich options
- D-Bus API
- Available for Linux and Windows
