mprime
======

## Links
Website: https://www.mersenne.org/

## Description
Prime95, also distributed as the command-line utility mprime for FreeBSD and Linux, is a freeware application written by George Woltman. It is the official client of the Great Internet Mersenne Prime Search, a volunteer computing project dedicated to searching for Mersenne primes. It is also used in overclocking to test for system stability.
