Protocol Buffers 3
==================

## Links
Website: https://github.com/google/protobuf
Documentation: https://protobuf.dev/

## Description
Protocol Buffers (a.k.a., protobuf) are Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data.
