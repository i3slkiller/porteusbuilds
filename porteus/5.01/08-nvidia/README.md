NVIDIA Accelerated Graphics Driver for Linux
============================================

## Links
Website: https://www.nvidia.com/Download/index.aspx

## Tested buildable driver versions
- 340.108
- 470.239.06
- 535.86.05
- 535.171.04
- 550.67
- 550.78

## Tested working driver versions
- N/A

## Parameters
- _COMPAT32=[yes|no] - install 32-bit libraries in module (default: yes)
