ZeroTier One
============

## Links
Website: https://www.zerotier.com/
Documentation: https://docs.zerotier.com/

## Description
ZeroTier is a smart programmable Ethernet switch for planet Earth. It allows all networked devices, VMs, containers, and applications to communicate as if they all reside in the same physical data center or cloud region.

## Notes
To start zerotier service at boot:
- ``chmod +x /etc/rc.d/rc.zerotier-one``
- add ``[ -x /etc/rc.d/rc.zerotier-one ] && /etc/rc.d/rc.zerotier-one start`` in ``/etc/rc.d/rc.local``
