RSS Guard
=========

## Links
Website: https://github.com/martinrotter/rssguard

## Description
RSS Guard is a simple RSS/ATOM feed reader for Windows, Linux, BSD, OS/2 or macOS which can work with RSS/ATOM/JSON/Sitemap feeds as well as many online feed services:
-  Feedly
-  Gmail
-  Google Reader API (Bazqux, FreshRSS, Inoreader, Miniflux, Reedah, The Old Reader and more)
-  Nextcloud News
-  Tiny Tiny RSS

RSS Guard is also podcast player as it can play everything via its built-in mpv-based (or ffmpeg-based) media player.
