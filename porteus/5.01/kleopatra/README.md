Kleopatra
=========

## Links
Website: https://www.kde.org/applications/utilities/kleopatra/
Documentation: https://docs.kde.org/stable5/en/kleopatra/kleopatra/index.html

## Description
Kleopatra is a certificate manager and GUI for GnuPG. The software stores your OpenPGP certificates and keys.

## Warning
In LXQT, ``GnuPG Log Viewer`` will not work because of missing ``libcanberra.so.0``, which exists in other DEs.
