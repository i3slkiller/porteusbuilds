mumble-server
=============

## Links
Website: https://www.mumble.info/
Documentation: https://www.mumble.info/documentation/

## Description
mumble-sever is the VoIP server component for Mumble. Murmur is installed in a system-wide fashion, but can also be run by individual users. Each mumble-server process supports multiple virtual servers, each with their own user base and channel list.  Administration of mumble-server is done through D-Bus.
