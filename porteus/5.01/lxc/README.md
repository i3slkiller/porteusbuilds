lxc
===

## Links
Website: https://linuxcontainers.org/lxc/introduction/
Documentation: https://linuxcontainers.org/lxc/documentation/

## Description
LXC is a userspace interface for the Linux kernel containment features. Through a powerful API and simple tools, it lets Linux users easily create and manage system or application containers.

### Features
Current LXC uses the following kernel features to contain processes:
- Kernel namespaces (ipc, uts, mount, pid, network and user)
- Apparmor and SELinux profiles
- Seccomp policies
- Chroots (using pivot_root)
- Kernel capabilities
- CGroups (control groups)

LXC containers are often considered as something in the middle between a chroot and a full fledged virtual machine. The goal of LXC is to create an environment as close as possible to a standard Linux installation but without the need for a separate kernel.

## Notes
This module require kernel with these control groups compiled:
- cpuset
- cpu
- cpuacct
- blkio
- memory
- devices
- freezer
- net_cls
- perf_event
- pids
- rdma
- misc

Otherwise containers may not start at all.

This module have also added default config for guest user to create unprivileged containers.
