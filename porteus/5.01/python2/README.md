python2
=======

## Links
Website: https://www.python.org/

## Description
Python is an interpreted, interactive, object-oriented programming language that combines remarkable power with very clear syntax. Python's basic power can be extended with your own modules written in C or C++. Python is also adaptable as an extension language for existing applications.

This module won't conflict with Python 3 in Porteus and Python 2 by default isn't used as default Python interpreter.
