florence
========

## Links
Website: https://florence.sourceforge.net/english.html
Documentation: https://florence.sourceforge.net/english/index.html

## Description
Florence is an extensible scalable virtual keyboard for X11. You need it if you can't use a real hardware keyboard, for example because you are disabled, your keyboard is broken or because you use a tablet PC, but you must be able to use a pointing device (as a mouse, a trackball, a touchscreen or opengazer); If you can't use a pointing device, there is gok, which can be used with just simple switches.

Florence stays out of your way when you don't need it: it appears on the screen only when you need it. A timer-based auto-click input method is available to help disabled people having difficulties to click. You may also check the new efficient ramble method.
