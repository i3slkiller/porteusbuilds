QuiteRSS
=========

## Links
Website: https://quiterss.org/

## Description
QuiteRSS is a open-source cross-platform RSS/Atom news feeds reader

Idea:
- Quite fast and comfortable to user

Features:
- Embedded browser (Webkit core)
- Feed and news filters: new, unread, starred, deleted (for news until restart application)
- User filters
- Proxy configuration: automatic or manual
- Feed import wizard: Search feed URL if site URL was entered
- Adblock
- Click to Flash
- Mark news starred
- Automatic update feeds: on startup, by timer
- Automatic cleanup on close using criterias
- Enable/Disable images in news preview
- Ability to quickly hide feed tree (for comfortable viewing)
- Open feed or news in own tab
- Quick news filter and quick search in browser
- Sound notification on new news
- Popup notification on new news
- Show new or unread news counter on tray icon
- Minimize on system tray: on start, on close, on minimize
- Import/Export feeds (OPML-files)
- Portable (Windows)
- Free working set (Windows)
- Shortcuts
- Check for updates
- Cross-platform
- Multilingual
- Open-source
